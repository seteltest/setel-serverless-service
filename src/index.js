require('module-alias/register');

// define all the routes/handlers
const validateSession = require('./handler/v1/validate-session');
const loginRoute = require('./handler/v1/login');

module.exports = {
  validateSession,
  loginRoute
};
