const path = require('path');

// import .env variables
require('dotenv-safe').load({
  path: path.join(process.cwd(), '.env'),
  sample: path.join(process.cwd(), '.env.example')
});

module.exports = {
  env: process.env.NODE_ENV,
  serviceName: 'setel-serverless-service',
  jwtSetting: {
    expiryTime: process.env.JWT_EXPIRATION_TIME,
    refreshTime: process.env.JWT_REFRESH_EXPIRATION_TIME,
    secret: process.env.JWT_SECRET
  }
};
