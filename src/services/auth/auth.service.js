/**
 * Auth Service
 *
 */
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const { jwtSetting } = require('@config/vars');
const { APIError } = require('@utils/APIError');

const PRINCIPAL_ID = 'user';
const fakeIds = {
  'khanna.mailme@gmail.com': '11781a85-fab6-4533-bad0-d93f815e0e2c',
  'user1@test.com': 'b53ca45f-9931-425c-8888-db78d69c2796',
  'user2@test.com': '50410232-dcb6-455b-b41d-b7deab82c8fb'
};

/**
 * Function to validate user credentails
 * This is a mock function to validate, in real scenario it will talk to database
 * @param {String} email               Email address of the user
 * @param {String} password            User Password
 */
/* istanbul ignore next */
const validateCredentials = async (email, password) => {
  if (email === 'admin@setel.com.my' && password === 'admin') {
    return { type: 'admin', userId: 'f76f378f-68d9-43ce-b77a-8256360a9c9d' };
  }
  if (Object.keys(fakeIds).indexOf(email) > -1 && password === 'demo') {
    return { type: 'user', userId: fakeIds[email] };
  }
  throw APIError.withCode('INVALID_LOGIN');
};

/**
 * Generates Lambda Authorizer Policy
 * @param {String} principalId  PrincipleID
 * @param {String} effect      'Allow' or 'Deny'
 * @param {String} resource     Resource Arn
 * @returns {Object} returns AWS Authorizer Policy
 */
const generateAuthPolicy = (principalId, effect, resource, payload = {}) => {
  const authResponse = {};
  authResponse.principalId = principalId;
  if (effect && resource) {
    const policyDocument = {};
    policyDocument.Version = '2012-10-17';
    policyDocument.Statement = [];
    const statementOne = {};
    statementOne.Action = 'execute-api:Invoke';
    statementOne.Effect = effect;
    statementOne.Resource = '*';
    policyDocument.Statement[0] = statementOne;
    authResponse.policyDocument = policyDocument;
  }

  authResponse.context = payload;

  return authResponse;
};

/**
 * Generates ALLOW Lambda Authorizer Policy
 * @param {String} resource     Resource Arn
 * @returns {Object} returns AWS Authorizer Policy
 */
const generateAuthAllowPolicy = (resource, decodedPayload) => generateAuthPolicy(PRINCIPAL_ID, 'Allow', resource, decodedPayload);

/**
 * Generates DENY Lambda Authorizer Policy
 * @param {String} resource     Resource Arn
 * @returns {Object} returns AWS Authorizer Policy
 */
const generateAuthDenyPolicy = resource => generateAuthPolicy(PRINCIPAL_ID, 'Deny', resource, {});

/**
 * Function verify JWT Signature
 * @param {String} token      JWT Token to be verified
 * @returns {Promise} returns Promise with decoded token
 */
const verifyToken = async (token) => {
  if (_.isEmpty(token)) {
    throw APIError.unauthorized();
  }

  try {
    return jwt.verify(token, jwtSetting.secret);
  } catch (e) {
    throw APIError.unauthorized();
  }
};

/**
 * Function to sign the token
 * @param {Object} payload
 * @param {Object} options
 */
const sign = (payload, options) => jwt.sign(payload, jwtSetting.secret, options);

/**
 * Function to generate JWT Tokens
 * @param {Object} payload      Payload to be signed
 * @returns {Object} returns object with tokens { token, refreshToken }
 */
const generateAuthToken = async (payload) => {
  const token = sign(payload, { expiresIn: jwtSetting.expiryTime });
  const refreshToken = sign(payload, { expiresIn: jwtSetting.refreshTime });
  return { token, refreshToken };
};

module.exports = {
  validateCredentials,
  generateAuthAllowPolicy,
  generateAuthDenyPolicy,
  generateAuthToken,
  verifyToken
};
