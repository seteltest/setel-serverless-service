const { OK } = require('@utils/helper');
const { validateCredentials, generateAuthToken } = require('@services/auth');

/**
 * login
 * @public
 */
exports.login = async (event) => {
  const { email, password } = event.body;
  const { type, userId } = await validateCredentials(email, password);
  const { token, refreshToken } = await generateAuthToken({ email, type, userId });
  return OK('Session Authorized', {
    token,
    refreshToken
  });
};
