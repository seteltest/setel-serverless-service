/* eslint-disable arrow-body-style */
jest.mock('@services/auth');
const { validateCredentials, generateAuthToken } = require('@services/auth');
const httpStatus = require('http-status');
const controller = require('./login.controller');

describe('Test login', () => {
  let event;

  beforeEach(() => {
    event = {
      body: {
        email: 'test@test.com',
        password: 'test'
      }
    };
  });

  afterEach(() => {
    event = undefined;
    jest.resetAllMocks();
  });

  it('should return valid tokens', async () => {
    validateCredentials.mockResolvedValue({ type: 'admin', userId: 1 });
    generateAuthToken.mockResolvedValue({ token: 'token', refreshToken: 'refresh' });
    const { statusCode, body } = await controller.login(event);
    expect(statusCode).toBe(httpStatus.OK);
    const result = JSON.parse(body);
    expect(result.responseCode).toBe(httpStatus.OK);
    expect(result.responseMessage).toBeString();
    expect(result.response).toBeObject();
    expect(result.response.token).not.toBeEmpty();
    expect(result.response.refreshToken).not.toBeEmpty();
  });
});
