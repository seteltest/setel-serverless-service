/* eslint-disable arrow-body-style */
const { generateAuthToken } = require('@services/auth');
const controller = require('./validate-session.controller');

describe('Test validateSession', () => {
  let event;
  const email = 'dummy@test.com';
  const type = 'admin';
  const userId = '1';
  const methodArn = 'randomArn';

  beforeEach(() => { });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should return Unauthorized Error', async () => {
    event = {
      authorizationToken: ''
    };
    const result = await controller.validateSession(event);
    expect(result).not.toHaveProperty('policyDocument');
    expect(result.context).toBeEmpty();
  });

  it('should return Unauthorized Error if token not valid', async () => {
    event = {
      authorizationToken: 'fakeToken'
    };
    const result = await controller.validateSession(event);
    expect(result).not.toHaveProperty('policyDocument');
    expect(result.context).toBeEmpty();
  });

  it('should return AWS Policy', async () => {
    const authTokens = await generateAuthToken({
      email, type, userId
    });
    event = {
      methodArn,
      authorizationToken: authTokens.token
    };
    const policy = await controller.validateSession(event);
    expect(policy).toBeObject();
    expect(policy).toHaveProperty('policyDocument');
    expect(policy.policyDocument).not.toBeEmpty();
    expect(policy).toHaveProperty('principalId');
    expect(policy.principalId).not.toBeEmpty();
  });
});
