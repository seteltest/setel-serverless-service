const { verifyToken, generateAuthAllowPolicy, generateAuthDenyPolicy } = require('@services/auth');
const logger = require('@utils/logger');

/**
  * Internal Lambda Authorizer
  *
  * Returns Lambda Authorizer Policy
  * @method login
  * @param {String} event.authorizationToken
  * @throws Returns 401 if the Client Params are invalid
  * @returns {Object} AWS Policy
  */
exports.validateSession = async (event) => {
  const token = event.authorizationToken;
  try {
    const decodedPayload = await verifyToken(token);
    logger.info('Authorizer', {
      token,
      decodedPayload
    });
    return generateAuthAllowPolicy(event.methodArn, decodedPayload);
  } catch (e) {
    logger.error('Authorizer', {
      token,
      err: e
    });
    return generateAuthDenyPolicy(event.methodArn);
  }
};
